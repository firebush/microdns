.. MicroDNS documentation master file, created by
   sphinx-quickstart on Mon Dec 10 14:39:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MicroDNS' documentation!
====================================

MicroDNS is a mock DNS used in conjunction with other tools (MicroServer, TrafficReplay) to test Apache Traffic Server. It is designed to provide a simple DNS interface to redirect domains to custom IPs for testing.

Installation
-----------------
MicroDNS depends on:

* `TRlib <https://pypi.org/project/trlib/>`_
* dnslib

Using MicroDNS
-----------------

The MicroDNS module provides the following functions:

**MicroDNS**\ (*zone_file, port, ip='127.0.0.1', rr=False*)
    * *zone_file* - JSON file containing the domains and IPs (required)
    * *port* - port for MicroDNS to run on (required)
    * *ip* - IP address for MicroDNS to run on (default: 127.0.0.1)
    * *rr* - if MicroDNS should round robin select IPs for a domain if there are multiple IPs present (default: False)

    These are also the same arguments needed to launch MicroDNS from the command line.

MicroDNS.\ **serve_forever**\ ()
    This will spawn 2 threads that launches a threading TCP server and a threading UDP server. 
    This function is called automatically if MicroDNS is launched from the command line; if MicroDNS is instantiated, the user will have to call this function manually to start the servers.
    MicroDNS will continue to run until it receives a SigINT. 


MicroDNS' *zone_file* is formmated as following:

.. code-block:: python

    { 
        "mappings": [{"abc.com": "127.0.0.2"}, {"foo.com": ["127.0.0.3", "127.0.0.4"]}, {"bar.com": "NXDOMAIN"}],
        "otherwise": ["127.0.0.1"]
    }

This configuration specifies the following behavior:
    * Resolve queries for ``abc.com`` to ``127.0.0.2``.
    * Resolve  queries for ``foo.com`` to the addresses ``127.0.0.3`` and ``127.0.0.4``. These addresses will be
      returned in every DNS response. If the *rr* option has been provided, then the order of these addresses will be
      shifted by one with each query (meaning that their order will alternate in this case since there are only two
      addresses).
    * Return the ``NXDOMAIN`` DNS response message type for queries against ``bar.com``, indicating that the IP address
      for ``bar.com`` could not be resolved.
    * The optional `otherwise` address list is used for all queries against domains not specified in the ``mappings``
      dictionary. Again, the *rr* optional argument will apply to this list as it would for any specified domain.

TODOs
-----------------

* Make TCP DNS server optional 



.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
